/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Backend;

import PaqueteMascotas.Hamster;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author valep
 */
public class AdminHamsters {
    
    ConexionBDD conexion = new ConexionBDD();
    
    Connection c;
    PreparedStatement ps;
    ResultSet rs;
    
    public boolean consultarHamsters(String nombre){
        
        String sql = "SELECT * FROM hamsters WHERE nombre="+"'"+nombre+"'";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            rs = ps.executeQuery();
            
        if(rs.next()){
            System.out.println("El nombre del hamster es:"+rs.getString("nombre"));
            System.out.println("El color del hamster es:"+rs.getString("color"));
            System.out.println("La altura del hamster es:"+rs.getString("altura"));
            System.out.println("El peso del hamster es:"+rs.getString("peso"));
            System.out.println("La edad del hamster es:"+rs.getString("edad"));
            
        }else{
            return false;
        }
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL CONSULTAR LA INFO DEL HAMSTER");
            
        }     
        return true;
    }
    
    public boolean agregarHamsters(Hamster hamster){
        
        String sql = "INSERT INTO hamsters (nombre, color, altura, peso, edad) VALUES(?, ?, ?, ?, ?)";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            
            ps.setString(1, hamster.getNombre());
            ps.setString(2, hamster.getColor());
            ps.setDouble(3, hamster.getAltura());
            ps.setDouble(4, hamster.getPeso());
            ps.setInt(5, hamster.getEdad());
            
            ps.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL AGREGAR LA INFO DEL HAMSTER");
            
        }     
        return true;
    }
    
    public boolean eliminarHamsters(String nombre){
        
        String sql = "DELETE FROM hamsters WHERE nombre="+"'"+nombre+"'";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            
            ps.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL ELIMINAR LA INFO DEL HAMSTER");
        
        }
        return true;
    }
    
    
}
