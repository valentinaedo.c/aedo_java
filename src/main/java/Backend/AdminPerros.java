/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Backend;

import PaqueteMascotas.Perro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author valep
 */
public class AdminPerros {
    
    ConexionBDD conexion = new ConexionBDD();
    
    Connection c;
    PreparedStatement ps;
    ResultSet rs;
    
    public boolean consultarPerros(String nombre){
        
        String sql = "SELECT * FROM perros WHERE nombre="+"'"+nombre+"'";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            rs = ps.executeQuery();
            
        if(rs.next()){
            System.out.println("El nombre del perrito es:"+rs.getString("nombre"));
            System.out.println("El color del perrito es:"+rs.getString("color"));
            System.out.println("La altura del perrito es:"+rs.getString("altura"));
            System.out.println("El peso del perrito es:"+rs.getString("peso"));
            System.out.println("La edad del perrito es:"+rs.getString("edad"));
            
        }else{
            return false;
        }
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL CONSULTAR LA INFO DEL PERRITO");
            
        }     
        return true;
    }
    
    public boolean agregarPerros(Perro perro){
        
        String sql = "INSERT INTO perros (nombre, color, altura, peso, edad) VALUES(?, ?, ?, ?, ?)";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            
            ps.setString(1, perro.getNombre());
            ps.setString(2, perro.getColor());
            ps.setDouble(3, perro.getAltura());
            ps.setDouble(4, perro.getPeso());
            ps.setInt(5, perro.getEdad());
            
            ps.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL AGREGAR LA INFO DEL PERRITO");
            
        }     
        return true;
    }
    
    public boolean eliminarPerros(String nombre){
        
        String sql = "DELETE FROM perros WHERE nombre="+"'"+nombre+"'";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            
            ps.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL ELIMINAR LA INFO DEL PERRITO");
            
        }
        return true;
    }
    
}