/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Backend;

import PaqueteMascotas.Gato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author valep
 */
public class AdminGatos {
    
    ConexionBDD conexion = new ConexionBDD();
    
    Connection c;
    PreparedStatement ps;
    ResultSet rs;
    
    public boolean consultarGatos(String nombre){
        
        String sql = "SELECT * FROM gatos WHERE nombre="+"'"+nombre+"'";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            rs = ps.executeQuery();
            
        if(rs.next()){
            System.out.println("El nombre del gatito es:"+rs.getString("nombre"));
            System.out.println("El color del gatito es:"+rs.getString("color"));
            System.out.println("La altura del gatito es:"+rs.getString("altura"));
            System.out.println("El peso del gatito es:"+rs.getString("peso"));
            System.out.println("La edad del gatito es:"+rs.getString("edad"));
            
        }else{
            return false;
        }
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL CONSULTAR LA INFO DEL GATITO");
            
        }     
        return true;
    }
    
    public boolean agregarGatos(Gato gato){
        
        String sql = "INSERT INTO gatos (nombre, color, altura, peso, edad) VALUES(?, ?, ?, ?, ?)";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            
            ps.setString(1, gato.getNombre());
            ps.setString(2, gato.getColor());
            ps.setDouble(3, gato.getAltura());
            ps.setDouble(4, gato.getPeso());
            ps.setInt(5, gato.getEdad());
            
            ps.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL AGREGAR LA INFO DEL GATITO");
            
        }     
        return true;
    }
    
    public boolean eliminarGatos(String nombre){
        
        String sql = "DELETE FROM gatos WHERE nombre="+"'"+nombre+"'";
        
        try{
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            
            ps.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("ERROR AL ELIMINAR LA INFO DEL GATITO");
        
        }
        return true;
    }
    
    
    
}
