/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Backend.AdminGatos;
import Backend.AdminHamsters;
import Backend.AdminPerros;
import PaqueteMascotas.Gato;
import PaqueteMascotas.Hamster;
import PaqueteMascotas.Perro;
import java.util.Scanner;
/**
 *
 * @author valep
 */
public class Menu {
    
    static AdminPerros adminperros = new AdminPerros();
    static AdminGatos admingatos = new AdminGatos();
    static AdminHamsters adminhamsters = new AdminHamsters();
    
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);

        int opcion;
        int respuesta;
        
        do{
        
        System.out.println("Ingrese la opción: ");
        System.out.println("1. - Agregar Perros");
        System.out.println("2. - Agregar Gatos");
        System.out.println("3. - Agregar Hamsters");
        System.out.println("4. - Consultar Info Perros");
        System.out.println("5. - Consultar Info Gatos");
        System.out.println("6. - Consultar Info Hamsters");
        System.out.println("7. - Eliminar Info Perros");
        System.out.println("8. - Eliminar Info Gatos");
        System.out.println("9. - Eliminar Info Hamsters");
        opcion = teclado.nextInt();
        System.out.println("");
        
        switch (opcion) {
            
            case 1:
                
                do{
                Perro lomito = new Perro();
                
                System.out.println("Agregando un perrito");
                System.out.println("Ingrese nombre");
                String nombre = teclado.next();
                
                System.out.println("Ingrese color");
                String color = teclado.next();
                
                System.out.println("Ingrese altura");
                double altura = teclado.nextDouble();
                
                System.out.println("Ingrese peso");
                double peso = teclado.nextDouble();
                
                System.out.println("Ingrese edad");
                int edad = teclado.nextInt();
                
                lomito.setNombre(nombre);
                lomito.setColor(color);
                lomito.setAltura(altura);
                lomito.setPeso(peso);
                lomito.setEdad(edad);
                
                boolean resp = adminperros.agregarPerros(lomito);
                if(resp==true){
                    System.out.println("¡INFORMACION AGREGADA CORRECTAMENTE!");
                }else{
                    System.out.println("ERROR AL AGREGAR LOS DATOS");
                }
                
                System.out.println("¿Desea agregar mas perritos? 1.- SI 2.- NO");
                respuesta = teclado.nextInt();
                
                }while(respuesta==1);
                
                break;
            
            case 2:
                
                do{
                
                Gato michi = new Gato();
                
                System.out.println("Agregando un gatito");
                System.out.println("Ingrese nombre");
                String nombre = teclado.next();
                
                System.out.println("Ingrese color");
                String color = teclado.next();
                
                System.out.println("Ingrese altura");
                double altura = teclado.nextDouble();
                
                System.out.println("Ingrese peso");
                double peso = teclado.nextDouble();
                
                System.out.println("Ingrese edad");
                int edad = teclado.nextInt();
                
                michi.setNombre(nombre);
                michi.setColor(color);
                michi.setAltura(altura);
                michi.setPeso(peso);
                michi.setEdad(edad);
                
                boolean resp = admingatos.agregarGatos(michi);
                if(resp==true){
                    System.out.println("¡INFORMACION AGREGADA CORRECTAMENTE!");
                }else{
                    System.out.println("ERROR AL AGREGAR LOS DATOS");
                }
                
                System.out.println("¿Desea agregar mas gatitos? 1.- SI 2.- NO");
                respuesta = teclado.nextInt();
                
                }while(respuesta==1);
                
                break;
                
            case 3:
                
                do{
                
                Hamster hamster = new Hamster();
                
                System.out.println("Agregando un Hamster");
                System.out.println("Ingrese nombre");
                String nombre = teclado.next();
                
                System.out.println("Ingrese color");
                String color = teclado.next();
                
                System.out.println("Ingrese altura");
                double altura = teclado.nextDouble();
                
                System.out.println("Ingrese peso");
                double peso = teclado.nextDouble();
                
                System.out.println("Ingrese edad");
                int edad = teclado.nextInt();
                
                hamster.setNombre(nombre);
                hamster.setColor(color);
                hamster.setAltura(altura);
                hamster.setPeso(peso);
                hamster.setEdad(edad);
                
                 boolean resp =adminhamsters.agregarHamsters(hamster);
                if(resp==true){
                    System.out.println("¡INFORMACION AGREGADA CORRECTAMENTE!");
                }else{
                    System.out.println("ERROR AL AGREGAR LOS DATOS");
                }

                System.out.println("¿Desea agregar mas hamsters? 1.- SI 2.- NO");
                respuesta = teclado.nextInt();
                
                }while(respuesta==1);
                                
                break;
                
            case 4:
                
                System.out.println("Ingrese el nombre del perrito");
                String nombre_perro = teclado.next();
                
                boolean resp_consultar = adminperros.consultarPerros(nombre_perro);
                
                if(resp_consultar==false){
                    System.out.println("PERRITO NO ENCONTRADO");
                }
                
                break;
                
            case 5:
                System.out.println("Ingrese el nombre del gatito");
                String nombre_gato = teclado.next();
                
                boolean resp_consulta = admingatos.consultarGatos(nombre_gato);
                
                if(resp_consulta==false){
                    System.out.println("GATITO NO ENCONTRADO");
                }
                                
                break;
                
            case 6:
                System.out.println("Ingrese el nombre del hamster");
                String nombre_hamster = teclado.next();
                
                boolean resp_consult = adminhamsters.consultarHamsters(nombre_hamster);
                
                if(resp_consult==false){
                    System.out.println("HAMSTER NO ENCONTRADO");
                }
                
                break;
                
            case 7:
                do{
                    boolean resp_eliminacion = false;
                    System.out.println("Ingrese el nombre del perrito a eliminar: ");
                    String eliminar_perrito = teclado.next();
                    
                    System.out.println("");
                    System.out.println("DATOS ENCONTRADOS: ");
                    
                    boolean resp_eliminar = adminperros.consultarPerros(eliminar_perrito);
                    if(resp_eliminar==false){
                        System.out.println("PERRITO NO ENCONTRADO");
                    }else{
                        System.out.println("Seguro que desea eliminar los datos de este perrito? 1.- SI 2.- NO");
                        int confirmacion = teclado.nextInt();
                        
                        if(confirmacion==1){
                            resp_eliminacion = adminperros.eliminarPerros(eliminar_perrito);
                        }
                        
                        if(resp_eliminacion==true){
                            System.out.println("PERRITO ELIMINADO EXITOSAMENTE!");
                        }
                    }
                    
                    System.out.println("Desea eliminar otra informacion de perritos? 1.- SI 2.- NO");
                    respuesta=teclado.nextInt();
                }while(respuesta==1);
                
                break;
                
            case 8:
                do{
                    boolean resp_eliminacion = false;
                    System.out.println("Ingrese el nombre del gatito a eliminar: ");
                    String eliminar_gatito = teclado.next();
                    
                    System.out.println("");
                    System.out.println("DATOS ENCONTRADOS: ");
                    
                    boolean resp_eliminar = admingatos.consultarGatos(eliminar_gatito);
                    if(resp_eliminar==false){
                        System.out.println("GATITO NO ENCONTRADO");
                    }else{
                        System.out.println("Seguro que desea eliminar los datos de este gatito? 1.- SI 2.- NO");
                        int confirmacion = teclado.nextInt();
                        
                        if(confirmacion==1){
                            resp_eliminacion = admingatos.eliminarGatos(eliminar_gatito);
                        }
                        
                        if(resp_eliminacion==true){
                            System.out.println("GATITO ELIMINADO EXITOSAMENTE!");
                        }
                    }
                    
                    System.out.println("Desea eliminar otra informacion de gatitos? 1.- SI 2.- NO");
                    respuesta=teclado.nextInt();
                }while(respuesta==1);
                
                break;
            
            case 9:
                do{
                    boolean resp_eliminacion = false;
                    System.out.println("Ingrese el nombre del hamster a eliminar: ");
                    String eliminar_hamster = teclado.next();
                    
                    System.out.println("");
                    System.out.println("DATOS ENCONTRADOS: ");
                    
                    boolean resp_eliminar = adminhamsters.consultarHamsters(eliminar_hamster);
                    if(resp_eliminar==false){
                        System.out.println("HAMSTER NO ENCONTRADO");
                    }else{
                        System.out.println("Seguro que desea eliminar los datos de este hamster? 1.- SI 2.- NO");
                        int confirmacion = teclado.nextInt();
                        
                        if(confirmacion==1){
                            resp_eliminacion = adminhamsters.eliminarHamsters(eliminar_hamster);
                        }
                        
                        if(resp_eliminacion==true){
                            System.out.println("HAMSTER ELIMINADO EXITOSAMENTE!");
                        }
                    }
                    
                    System.out.println("Desea eliminar otra informacion de perritos? 1.- SI 2.- NO");
                    respuesta=teclado.nextInt();
                }while(respuesta==1);
                
                break;
                
            default:
            System.out.println("OPCION INVÁLIDA, GRACIAS, VUELVAS PRONTO");  
        }
            
            System.out.println("");
            System.out.println("Volver al menú? 1.- SI 2.- NO");
            opcion = teclado.nextInt();
            
        }while(opcion==1);    
        
    }
        
}